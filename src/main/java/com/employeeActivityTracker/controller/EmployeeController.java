package com.employeeActivityTracker.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.employeeActivityTracker.dto.EmployeeDto;
import com.employeeActivityTracker.service.EmployeeService;

@Validated
@RestController
@RequestMapping("/api/Employee")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	@PostMapping("/add")
	public ResponseEntity<String> addEmployee(
			@Valid
			@RequestBody
			@NotEmpty(message = "Employee cannot be empty")  List<@Valid EmployeeDto> employeeDtos) {
		boolean isSaved = employeeService.addEmployee(employeeDtos);
		if (isSaved)
			return new ResponseEntity<>("Successfully Added Employee(s)", HttpStatus.CREATED);
		return new ResponseEntity<>("Failed to add employee(s)", HttpStatus.BAD_REQUEST);
	}

	@GetMapping("")
	public ResponseEntity<List<EmployeeDto>> getAllEmployeeDetails(@RequestParam int pageSize,
			@RequestParam int pageNumber) {
		return new ResponseEntity<List<EmployeeDto>>(employeeService.getAllEmployeeDetails(pageSize,pageNumber), HttpStatus.OK);
	}

	@GetMapping("{employeeCode}")
	public ResponseEntity<EmployeeDto> getEmployeeByCode(@PathVariable Long employeeCode) {
		return new ResponseEntity<EmployeeDto>(employeeService.getEmployee(employeeCode), HttpStatus.OK);
	}

	@DeleteMapping("{employeeCode}")
	public ResponseEntity<String> deleteEmployee(@PathVariable Long employeeCode) {
		boolean isMarkedAsDeleted = employeeService.deleteEmployee(employeeCode);
		if (isMarkedAsDeleted)
			return new ResponseEntity<>("Successfully Marked Employee as deleted", HttpStatus.OK);
		return new ResponseEntity<>("Failed to mark delete employee", HttpStatus.BAD_REQUEST);
	}

	@PatchMapping("{employeeCode}")
	public ResponseEntity<String> modifyEmployee(@RequestBody Map<String, Object> updates,
			@PathVariable Long employeeCode) {
		System.out.println(updates.size());

		boolean isUpdated = employeeService.modifyEmployee(updates, employeeCode);
		if (isUpdated)
			return new ResponseEntity<>("Successfully modified employee", HttpStatus.OK);

		return new ResponseEntity<>("Failed to modify employee", HttpStatus.BAD_REQUEST);
	}
}
