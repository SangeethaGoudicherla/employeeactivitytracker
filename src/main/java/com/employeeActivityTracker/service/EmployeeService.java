package com.employeeActivityTracker.service;


import java.util.List;
import java.util.Map;

import com.employeeActivityTracker.dto.EmployeeDto;

public interface EmployeeService {

	boolean addEmployee(List<EmployeeDto> employeeDtos);

	List<EmployeeDto> getAllEmployeeDetails(int pageSize, int pageNumber);

	EmployeeDto getEmployee(Long employeeCode);

	boolean deleteEmployee(Long employeeCode);

	boolean modifyEmployee(Map<String, Object> updates, Long employeeCode);

}
