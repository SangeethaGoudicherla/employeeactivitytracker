package com.employeeActivityTracker.exception;

public enum ErrorMessage {
	EMPLOYEENOTFOUNDEXCEPTION("Employee doesnot exist in Database");
	
	private String message;

	public String getMessage() {
		return message;
	}

	private ErrorMessage(String message) {
		this.message = message;
	}

}
