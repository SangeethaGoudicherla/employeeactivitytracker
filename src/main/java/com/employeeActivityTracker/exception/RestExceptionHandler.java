package com.employeeActivityTracker.exception;

import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.employeeActivityTracker.dto.ErrorResponse;


@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler{
	
	private static final String EMPLOYEENOTFOUNDCODE = "Employee-001";
	private static final String INVALIDDATA = "InvalidData";
	private static final String ALLFIELDSERROR = "Employee Fields Error";
	
	@ExceptionHandler(value = EmployeeNotFoundException.class)
	public ResponseEntity<ErrorResponse> handleCustomerNotFoundException(EmployeeNotFoundException employeeException) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setStatusCode(EMPLOYEENOTFOUNDCODE);
		errorResponse.setStatusMessage(employeeException.getMessage());
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.BAD_REQUEST);
	}
	
	@Override
	public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException argInvalidException,
			HttpHeaders headers, HttpStatus httpStatus, WebRequest request) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setStatusCode(INVALIDDATA);
		String allFieldErrors = argInvalidException.getBindingResult().getFieldErrors().stream()
				.map(e -> e.getDefaultMessage()).collect(Collectors.joining(", "));
		errorResponse.setStatusMessage(allFieldErrors);
		return new ResponseEntity<Object>(errorResponse, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<ErrorResponse> handle(ConstraintViolationException constraintViolationException) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setStatusCode(ALLFIELDSERROR);
	    Set<ConstraintViolation<?>> violations = constraintViolationException.getConstraintViolations();
	    String errorMessage = "";
	    if (!violations.isEmpty()) {
	        StringBuilder builder = new StringBuilder();
	        violations.forEach(violation -> builder.append(" ," + violation.getMessage()));
	        errorMessage = builder.toString();
	    } else {
	        errorMessage = "ConstraintViolationException occured.";
	    }
	    errorResponse.setStatusMessage(errorMessage.substring(2));
	    return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	 }

}
