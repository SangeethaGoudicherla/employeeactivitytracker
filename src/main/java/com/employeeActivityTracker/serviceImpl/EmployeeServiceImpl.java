package com.employeeActivityTracker.serviceImpl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.employeeActivityTracker.dto.EmployeeDto;
import com.employeeActivityTracker.entity.Employee;
import com.employeeActivityTracker.exception.EmployeeNotFoundException;
import com.employeeActivityTracker.exception.ErrorMessage;
import com.employeeActivityTracker.repository.EmployeeRepository;
import com.employeeActivityTracker.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	private static final String INACTIVE = "InActive";
	private static final String ACTIVE = "Active";

	@Override
	public boolean addEmployee(List<EmployeeDto> employeeDtos) {
		// TODO Auto-generated method stub
		employeeDtos.stream().forEach(employeeDto -> {
			Employee employee = new Employee();
			BeanUtils.copyProperties(employeeDto, employee);
			employee.setStatus(ACTIVE);
			employeeRepository.save(employee);
		});

		return true;
	}

	@Override
	public List<EmployeeDto> getAllEmployeeDetails(int pageSize, int pageNumber) {
		List<EmployeeDto> employeeDtos = new ArrayList<>();

		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		List<Employee> employees = employeeRepository.findAll(pageable).getContent();
		
		//Picking only active employees
		List<Employee> activeEmployees = employees.stream() // converting the list to stream
				.filter(employee -> employee.getStatus().equals("Active")) // filter the stream to create a new stream
				.collect(Collectors.toList()); // collect the final stream and convert it to a List

		if (employees.size() == 0)
			throw new EmployeeNotFoundException(ErrorMessage.EMPLOYEENOTFOUNDEXCEPTION.getMessage());
		activeEmployees.stream().forEach(employee -> {
			EmployeeDto employeeDto = new EmployeeDto();
			BeanUtils.copyProperties(employee, employeeDto);
			employeeDtos.add(employeeDto);
		});

		return employeeDtos;
	}

	@Override
	public EmployeeDto getEmployee(Long employeeCode) {
		// TODO Auto-generated method stub
		Optional<Employee> optionalEmployee = employeeRepository.findById(employeeCode);

		if (!optionalEmployee.isPresent())
			throw new EmployeeNotFoundException(ErrorMessage.EMPLOYEENOTFOUNDEXCEPTION.getMessage());

		EmployeeDto employeeDto = new EmployeeDto();
		BeanUtils.copyProperties(optionalEmployee.get(), employeeDto);

		return employeeDto;

	}

	@Override
	public boolean deleteEmployee(Long employeeCode) {
		// TODO Auto-generated method stub
		Employee employee = new Employee();
		Optional<Employee> optionalEmployee = employeeRepository.findById(employeeCode);

		if (!optionalEmployee.isPresent())
			throw new EmployeeNotFoundException(ErrorMessage.EMPLOYEENOTFOUNDEXCEPTION.getMessage());

		employee = optionalEmployee.get();
		employee.setStatus(INACTIVE);
		employeeRepository.save(employee);

		return true;
	}

	@Override
	public boolean modifyEmployee(Map<String, Object> updates, Long employeeCode) {
		// TODO Auto-generated method stub
		Employee employee = new Employee();
		Optional<Employee> optionalEmployee = employeeRepository.findById(employeeCode);
		if (!optionalEmployee.isPresent())
			throw new EmployeeNotFoundException(ErrorMessage.EMPLOYEENOTFOUNDEXCEPTION.getMessage());

		employee = optionalEmployee.get();

		for (Map.Entry<String, Object> entry : updates.entrySet()) {
			try {
				org.apache.commons.beanutils.BeanUtils.setProperty(employee, entry.getKey(), entry.getValue());
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		employeeRepository.save(employee);
		return true;
	}

}
