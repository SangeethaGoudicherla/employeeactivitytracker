package com.employeeActivityTracker.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class EmployeeDto {

	@Digits(integer = 10, fraction = 1, message = "Employee Code should be less than 10 digits")
	private Long code;

	@NotEmpty(message = "Employee name should not be empty")
	@Size(min = 2, max = 100, message = "Employee name size must be between 2 and 100")
	private String name;

	@NotEmpty(message = "Employee gender should not be empty")
	@Size(max = 1, message = "Employee gender size must be one character")
	private String gender;

	@NotEmpty(message = "Employee designation should not be empty")
	@Size(min = 5, max = 100, message = "Employee designation size must be between 5 and 100")
	private String designation;

	@NotEmpty(message = "Employee emailId should not be empty")
	@Email
	private String emailId;

	@Digits(integer = 2, fraction = 2, message = "Employee Experience should be maximum of 2 digits")
	private Integer experience;

	@NotEmpty(message = "Employee phone number should not be empty")
	@Pattern(regexp = "^[0-9]{10}$",message="Phone number should be of 10 digits")
	private String phoneNumber;

	@NotEmpty(message = "Employee location should not be empty")
	@Size(min = 5, max = 100, message = "Employee location size must be between 5 and 100")
	private String location;

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Integer getExperience() {
		return experience;
	}

	public void setExperience(Integer experience) {
		this.experience = experience;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
